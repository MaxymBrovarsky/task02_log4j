package com.brom.task02;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  static final Logger logger = LogManager.getLogger(Main.class.getName());
  public static void main(String[] args) throws InterruptedException{
    logger.trace("trace");
    logger.debug("debug");
    logger.info("info");
    logger.warn("warn");
    logger.error("error");
    logger.fatal("fatal");
    while (true) {
      logger.info("info");
      Thread.sleep(10000);
    }
//    Sample s = new Sample();
  }
}
