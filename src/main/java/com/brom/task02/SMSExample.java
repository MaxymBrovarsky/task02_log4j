package com.brom.task02;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSExample {
  public static final String ACCOUNT_SID = "*";
  public static final String AUTH_TOKEN = "*";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message.creator(
        new PhoneNumber("*"),
        new PhoneNumber("*"),
        str).create();
  }

}
